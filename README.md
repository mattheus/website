# Muh site
My personal website. Contains the essentials, articles and contact. Themed using the gorgeous [Mountain](https://github.com/https://github.com/pradyungn/Mountain) theme.

![Preview](assets/preview.png)
